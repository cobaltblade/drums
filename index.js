var n_b = document.querySelectorAll(".drum").length;
//mosue click detection
for (var i=0;i<n_b;i++)
{
  document.querySelectorAll(".drum")[i].addEventListener("click",function(){
    var btnInnerHtml=this.innerHTML;
    soundHandler(btnInnerHtml);
    animationHandler(btnInnerHtml);
  });
}
//keypress Detection
document.addEventListener("keypress", function(event) {

  soundHandler(event.key);

  animationHandler(event.key);

});
function playAudio(filename)
{
  var audio=new Audio("sounds/"+filename);
  audio.play();
}
function soundHandler(key)
{
      switch (key)
      {
        case "w":
            playAudio("tom-1.mp3");
            break;
        case "a":
            playAudio("tom-2.mp3");
            break;
        case "s":
            playAudio("tom-3.mp3");
            break;
        case "d":
            playAudio("tom-4.mp3");
            break;
        case "j":
            playAudio("crash.mp3");
            break;
        case "k":
            playAudio("kick-bass.mp3");
            break;
        case "l":
            playAudio("snare.mp3");
            break;
      }
}



function animationHandler(key) {

  var activeButton = document.querySelector("." + key);

  activeButton.classList.add("pressed");

  setTimeout(function() {
    activeButton.classList.remove("pressed");
  }, 100);
}
